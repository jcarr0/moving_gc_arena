/*
 * @Copyright 2020 Jason Carr
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#[allow(unused)]
#[cfg(not(feature = "packed-headers"))]
mod safe_entry;
#[allow(unused)]
#[cfg(feature = "packed-headers")]
mod unsafe_entry;

#[cfg(not(feature = "packed-headers"))]
pub use safe_entry::*;

#[cfg(feature = "packed-headers")]
pub use unsafe_entry::*;
