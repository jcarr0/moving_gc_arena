/*
 * @Copyright 2020 Jason Carr
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use alloc::rc;
use alloc::rc::Rc;
use core::cell::Cell;
use core::fmt::{Debug, Formatter};
use core::hint::unreachable_unchecked;
use core::marker::PhantomData;
use core::mem::{forget, ManuallyDrop, MaybeUninit};

use crate::region::traverse;
use crate::types::{Ix, IxCell, Root, SpotVariant, Weak};

#[inline(always)]
unsafe fn invariant_unreachable() {
    if cfg!(debug_assertions) {
        unreachable!()
    } else {
        unreachable_unchecked()
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
//
// The present data contains a usize.
// Values of this type must have bottom
// bit 0.
//
// If the collector is not marking, then the bottom
// two bits must be 00.
//
// If the collector is marking, the bottom two bits
// can also be 10, to indicate a mark
//
// The value, once it is aligned by ensuring that
// the bottom two bits are 00, will be one of two
// things:
// 0,
// or a valid
// pointer to a *const IxCell<T> which was
// created by Rc::into_raw on the correct type.
// That IxCell<T> will be the
//
struct PresentData(usize);
impl PresentData {
    // NOTE: Although this type is Copy
    // This effectively should consume, so the Rc will need
    // to be forgotten if this is only a borrow
    #[inline]
    unsafe fn into_unchecked<T>(self) -> (bool, Option<*const IxCell<T>>) {
        (
            2 == (self.0 & 2usize),
            match self.0 & (!3usize) {
                // i.e. self.0 looks like:
                // 0000000000000...0000 or
                // 0000000000000...0010
                // In either case we consider it null
                0 => None,
                ptr => Some(ptr as *const IxCell<T>),
            },
        )
    }
    #[inline]
    unsafe fn from_unchecked<T>(mark: bool, rc: Option<*const IxCell<T>>) -> Self {
        PresentData(
            (if mark { 2usize } else { 0usize })
                | match rc {
                    Some(rc) => rc as usize,
                    None => 0usize,
                },
        )
    }
}

/**
 * A BrokenHeart contains an index that this entry
 * has been relocated to.
 *
 * We likewise assume that the bottom two bits are
 * available for our purposes.
 */
#[repr(C)]
#[derive(Clone, Copy)]
struct BrokenHeart(
    usize,
    #[cfg(feature = "debug-arena")] u64,
    #[cfg(feature = "debug-arena")] u64,
);
impl BrokenHeart {
    #[inline]
    unsafe fn into_unchecked<T>(self) -> Ix<T> {
        assert!(self.0 & 1 == 1);
        Ix::new(
            self.0.wrapping_shr(1),
            #[cfg(feature = "debug-arena")]
            self.1,
            #[cfg(feature = "debug-arena")]
            self.2,
        )
    }
    #[inline]
    fn from_unchecked<T>(ix: Ix<T>) -> Self {
        let val = ix.ix().wrapping_shl(1) | 1usize;
        assert!(val & 1 == 1);
        BrokenHeart(
            val,
            #[cfg(feature = "debug-arena")]
            ix.nonce,
            #[cfg(feature = "debug-arena")]
            ix.generation,
        )
    }
}

/**
 * Unsafe header. May be smaller and more performant,
 * but less-obviously correct, and making more assumptions
 * about architecture.
 *
 * We assume the following of the architecture:
 * Pointers are 4-byte aligned.
 * The all-zero pointer is not a valid pointer.
 *
 * The small size is necessary for some use cases, since
 * it fits in the same size as an Ix<T>
 */
#[repr(C)] // necessary to have correct size
pub union Header<T> {
    present: PresentData,
    broken_heart: BrokenHeart,
    bits: usize, // raw view, no invariants
    _phantom: PhantomData<*const Root<T>>,
}
enum TaggedHeader<T> {
    Present(bool, Option<*const IxCell<T>>),
    BrokenHeart(Ix<T>, MaybeUninit<T>),
}
impl<T> Default for TaggedHeader<T> {
    fn default() -> TaggedHeader<T> {
        TaggedHeader::Present(false, None)
    }
}
impl<T> PartialEq for Header<T> {
    fn eq(&self, other: &Self) -> bool {
        unsafe { self.bits == other.bits }
    }
}
impl<T> Eq for Header<T> {}
impl<T> Clone for Header<T> {
    fn clone(&self) -> Self {
        *self
    }
}
impl<T> Copy for Header<T> {}
impl<T> Default for Header<T> {
    fn default() -> Self {
        Header { bits: 1 }
    }
}

impl<T> Header<T> {
    //
    // NOTE for safety: Throughout these methods,
    // the correct representation is needed,
    // else the Header::sort method is immediate
    // UB
    //
    // The method itself is not unsafe as it
    // not exposed outside this module
    //

    // Header MUST point to present value
    fn root(this: &Cell<Self>, ix: Ix<T>) -> Root<T> {
        let cell: Rc<IxCell<T>> = Self::use_tag(this, |v| match v {
            TaggedHeader::Present(m, Some(ptr)) => unsafe {
                let rc = Rc::from_raw(ptr);
                let rc2 = rc.clone();
                let ptr = Rc::into_raw(rc);
                (TaggedHeader::Present(m, Some(ptr)), rc2)
            },
            TaggedHeader::Present(m, None) => {
                let rc = Rc::new(Cell::new(ix));
                let ptr = Rc::into_raw(rc.clone());
                (TaggedHeader::Present(m, Some(ptr)), rc)
            }
            _ => panic!("Invalid header state"),
        });
        Root { cell }
    }

    fn weak(this: &Cell<Self>, ix: Ix<T>) -> Weak<T> {
        Self::root(this, ix).weak()
    }

    #[inline(always)]
    fn present() -> Self {
        Header {
            present: PresentData(0),
        }
    }

    #[inline(always)]
    fn broken_heart(ix: Ix<T>) -> Self {
        Header {
            broken_heart: BrokenHeart::from_unchecked(ix),
        }
    }

    #[inline]
    fn use_tag<F, O>(this: &Cell<Self>, f: F) -> O
    where
        F: FnOnce(TaggedHeader<T>) -> (TaggedHeader<T>, O),
    {
        let mut this_val = this.take();
        // must have type invariants above
        let tagged: TaggedHeader<T> = unsafe { this_val.get_tag() };
        let (tagged, ret) = f(tagged);
        unsafe {
            match tagged {
                TaggedHeader::Present(m, rc) => {
                    this_val.present = PresentData::from_unchecked(m, rc)
                }
                TaggedHeader::BrokenHeart(bh, _) => {
                    this_val.broken_heart = BrokenHeart::from_unchecked(bh)
                }
            }
        };
        this.set(this_val);
        ret
    }

    // Always inline as the match block will
    // benefit from MutPresent invariants
    #[inline(always)]
    unsafe fn get_tag(&self) -> TaggedHeader<T> {
        unsafe {
            match self.bits & 1usize {
                0 => {
                    let (m, raw) = PresentData::into_unchecked(self.present);
                    TaggedHeader::Present(m, raw)
                }
                1 => TaggedHeader::BrokenHeart(
                    BrokenHeart::into_unchecked(self.broken_heart),
                    MaybeUninit::uninit(),
                ),
                _ => unreachable!(),
            }
        }
    }
}

/**
 * A spot consists of a header, and some optional data
 * that might contain a T.
 *
 * NOTE: As a safety invariant,
 * the value is initialized whenever
 * Header<T>.present is valid (as PresentData, not as a usize)
 *
 * NOTE: the Header's UnsafeCell will only be modified if it starts
 * as MutPresent, and ends as MutPresent. We cannot move the UnsafeCell
 * further in due to union's requirng Copy.
 * Although the behavior of certain methods may be independent,
 * we still cannot violate Rust's guarantees if we make references.
 *
 * The value may also be initialized in other situations as
 * required by algorithms, but importantly, those cases will
 * fail to drop, so the value should never be dropped while
 *
 */
pub(crate) struct Spot<T> {
    header: Cell<Header<T>>,
    value: MaybeUninit<T>,
}
impl<T> Drop for Spot<T> {
    fn drop(&mut self) {
        unsafe {
            if let TaggedHeader::Present(_, ptr) = self.get_tag() {
                // drop contents
                core::ptr::drop_in_place(self.value.as_mut_ptr());
                // drop rc
                if let Some(ptr) = ptr {
                    Rc::from_raw(ptr);
                    //drop
                };
            }
        }
    }
}

impl<T> Spot<T> {
    pub(crate) fn new(t: T) -> Self {
        Spot {
            header: Cell::new(Header::present()),
            value: MaybeUninit::new(t),
        }
    }

    // reads the header
    unsafe fn get_tag(&self) -> TaggedHeader<T> {
        self.header.get().get_tag()
    }

    unsafe fn get_tag_from_raw(this: *const Self) -> TaggedHeader<T> {
        (*this).header.get().get_tag()
    }

    pub(crate) fn get(&self) -> Option<Present<T>> {
        unsafe {
            match self.get_tag() {
                TaggedHeader::Present(_, _) => Some(Present::from_unchecked(self)),
                _ => None,
            }
        }
    }
    pub(crate) fn get_mut(&mut self) -> Option<MutPresent<T>> {
        unsafe {
            match self.get_tag() {
                TaggedHeader::Present(_, _) => Some(MutPresent::from_unchecked(self)),
                _ => None,
            }
        }
    }
    pub(crate) fn move_to(&mut self, other: Ix<T>) -> Spot<T> {
        if let Some(mut e) = self.get_mut() {
            e.move_to(other);
        };
        core::mem::replace(
            self,
            Spot {
                header: Cell::new(Header::broken_heart(other)),
                value: MaybeUninit::uninit(),
            },
        )
    }
    pub(crate) fn variant(&mut self) -> SpotVariant<MutPresent<T>, T> {
        unsafe {
            match self.get_tag() {
                TaggedHeader::Present(_, rc) => {
                    SpotVariant::Present(MutPresent::from_unchecked(self))
                }
                TaggedHeader::BrokenHeart(i, _) => SpotVariant::BrokenHeart(i),
            }
        }
    }

    pub(crate) unsafe fn is_forwarding(this: *const Self) -> Option<Ix<T>> {
        match Self::get_tag_from_raw(this) {
            TaggedHeader::BrokenHeart(i, _) => Some(i),
            _ => None,
        }
    }

    pub(crate) unsafe fn header_as_ix(&mut self) -> &mut Ix<T> {
        unsafe { &mut *(&mut self.header as *mut _ as *mut Ix<T>) }
    }

    // Marking functions.
    //
    // Both of these use raw pointers so
    // that we don't have overlapping mutable borrows on
    // a particular spot (specifically, they would overlap
    // at the instance of T; we still need disjointness
    // of the header from T)
    //
    pub(crate) unsafe fn mark(&mut self, mark: bool) {
        if let TaggedHeader::Present(_, ptr) = self.get_tag() {
            self.header.set(Header {
                present: PresentData::from_unchecked(mark, ptr),
            });
        }
    }

    pub(crate) unsafe fn is_marked(this: *const Self) -> Option<bool> {
        match Self::get_tag_from_raw(this) {
            TaggedHeader::Present(m, _) => Some(m),
            _ => None,
        }
    }

    // dfs mark can't borrow the entire spot,
    // has to borrow from the entry proper for disointness
    // with mark borrows
    pub(crate) unsafe fn get_mut_from_raw<'a>(this: *mut Self) -> Option<MutPresent<'a, T>> {
        unsafe {
            match Self::get_tag_from_raw(this) {
                TaggedHeader::Present(_, _) => Some(MutPresent::from_unchecked(&mut *this)),
                _ => None,
            }
        }
    }

    pub(crate) unsafe fn recover_kept(&mut self, mark: bool, mut dest_header: MutPresent<T>) {
        // Incredibly unsafe, all of this
        Cell::set(
            &self.header,
            Cell::replace(dest_header.header, Header::present()),
        );
        self.mark(mark);
    }

    pub(crate) fn move_to_keep_with<F>(&mut self, other: Ix<T>, mut clone: F) -> Option<Spot<T>>
    where
        F: FnMut(MutPresent<T>) -> Option<T>,
    {
        let value = if let Some(mut e) = self.get_mut() {
            clone(e)
        } else {
            unreachable!("Invalid GC internal state");
        };
        value.map(|value| {
            let header = core::mem::replace(&mut self.header, Cell::new(Header::broken_heart(other)));
            Spot {
                header,
                value: MaybeUninit::new(value),
            }
        })
    }
}

impl<T: Clone> Spot<T> {
    pub(crate) fn move_to_keep(&mut self, other: Ix<T>) -> Spot<T> {
        self.move_to_keep_with(other, |e| Some(e.get().clone())).expect("GC internal error: should be some for clone")
    }
}

// NOTE for safety: Header *must*
// be present and data *must*
// be initialized
//
// A spot that is guaranted to be present
// (both header and value).
//
// Although we can't enforce type invaraints,
// every method ensures the invariants at each
// point
//
/**
 * Entry which contains a valid instance of T
 * with full header.
 *
 * It implements traverse::Entry
 */
pub(crate) struct MutPresent<'a, T> {
    header: &'a Cell<Header<T>>,
    value: &'a mut T,
    _phantom: PhantomData<&'a mut Spot<T>>,
}
#[derive(Clone, Copy)]
pub(crate) struct Present<'a, T> {
    header: &'a Cell<Header<T>>,
    value: &'a T,
    _phantom: PhantomData<&'a Spot<T>>,
}
impl<'a, T> MutPresent<'a, T> {
    unsafe fn from_unchecked(spot: &'a mut Spot<T>) -> Self {
        let header = &spot.header;
        let value = &mut *(&mut spot.value as *mut _ as *mut T);
        MutPresent {
            header,
            value,
            _phantom: PhantomData,
        }
    }

    pub(crate) fn as_spot_mut(&mut self) -> &mut Spot<T> {
        unsafe { &mut *(self as *mut _ as *mut Spot<T>) }
    }

    pub(crate) fn move_to(&mut self, other: Ix<T>) {
        unsafe {
            match self.header.get().get_tag() {
                TaggedHeader::Present(_, Some(ptr)) => {
                    let rc = Rc::from_raw(ptr);
                    rc.set(other);
                    let _ = Rc::into_raw(rc);
                }
                TaggedHeader::Present(_, None) => (),
                _ => unreachable!(),
            }
        }
    }
    pub(crate) fn check_clear_rc(&mut self) {
        unsafe {
            Header::use_tag(self.header, |v: TaggedHeader<T>| {
                match v {
                    TaggedHeader::Present(m, Some(ptr)) => {
                        let cell = Rc::from_raw(ptr);
                        let root = Root { cell };
                        if !root.is_otherwise_rooted() && !root.is_weaked() {
                            (TaggedHeader::Present(m, None), ())
                        // rc dropped
                        } else {
                            // rc into pointer
                            let ptr = Rc::into_raw(root.cell);
                            (TaggedHeader::Present(m, Some(ptr)), ())
                        }
                    }
                    v => (v, ()),
                }
            });
        }
    }

    pub(crate) fn weak(&self, ix: Ix<T>) -> Weak<T> {
        unsafe { Header::weak(self.header, ix) }
    }

    pub(crate) fn root(&self, ix: Ix<T>) -> Root<T> {
        unsafe { Header::root(self.header, ix) }
    }

    #[inline(always)]
    pub fn get(&self) -> &T {
        &self.value
    }

    #[inline(always)]
    pub fn get_mut(mut self) -> &'a mut T {
        self.value
    }

    #[inline(always)]
    pub fn reborrow(&mut self) -> MutPresent<T> {
        let MutPresent { header, value, .. } = self;
        MutPresent {
            header,
            value,
            _phantom: PhantomData,
        }
    }
}

impl<'a, T> Present<'a, T> {
    unsafe fn from_unchecked(spot: &'a Spot<T>) -> Self {
        let header = &spot.header;
        let value = &*(&spot.value as *const _ as *const T);
        Present {
            header,
            value,
            _phantom: PhantomData,
        }
    }

    #[inline(always)]
    pub fn get(self) -> &'a T {
        self.value
    }

    pub(crate) fn weak(&self, ix: Ix<T>) -> Weak<T> {
        unsafe { Header::weak(self.header, ix) }
    }

    pub(crate) fn root(&self, ix: Ix<T>) -> Root<T> {
        unsafe { Header::root(self.header, ix) }
    }
}
