/*
 * @Copyright 2020 Jason Carr
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use super::Ix;
use alloc::{boxed::Box, vec::Vec};

/**
 * Trait to expose contained indices to the garbage collector.
 */
pub trait HasIx<T: 'static> {
    /**
     * Expose a mutable reference to every Ix owned
     * by this datastructure.
     *
     * This is used for the collector to know which
     * other elements are pointed to from this element,
     * as well as to update all of the old-generation
     * indices to the new generation.
     *
     * Avoid panicking in this method. A panic may
     * cause some elements to never be dropped, leaking
     * any owned memory outside the region.
     *
     * The function f should be called exactly once
     * on each Ix<T> value. With the debug-arena flag on,
     * region mismatches will trigger a panic.
     * In some rare cases, an index itself will be shared
     * between different elements (such as with
     * Rc<Cell<Ix<T>>>). In these situations, care should
     * be taken that each index is exposed only once no
     * matter how many elements are passed over, but this
     * use case is currently not well supported.
     */
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, f: F)
    where
        F: FnMut(&'b mut Ix<T>);
}
impl<T: 'static, S: HasIx<T>> HasIx<T> for Vec<S> {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        self.iter_mut().for_each(|o| o.foreach_ix(&mut f));
    }
}
impl<T: 'static> HasIx<T> for () {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut _f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
    }
}
impl<T: 'static, S1: HasIx<T>, S2: HasIx<T>> HasIx<T> for (S1, S2) {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        self.0.foreach_ix(&mut f);
        self.1.foreach_ix(&mut f);
    }
}
impl<T: 'static, S1: HasIx<T>, S2: HasIx<T>, S3: HasIx<T>> HasIx<T> for (S1, S2, S3) {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        self.0.foreach_ix(&mut f);
        self.1.foreach_ix(&mut f);
        self.2.foreach_ix(&mut f);
    }
}
impl<T: 'static, S1: HasIx<T>, S2: HasIx<T>, S3: HasIx<T>, S4: HasIx<T>> HasIx<T>
    for (S1, S2, S3, S4)
{
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        self.0.foreach_ix(&mut f);
        self.1.foreach_ix(&mut f);
        self.2.foreach_ix(&mut f);
        self.3.foreach_ix(&mut f);
    }
}
impl<T: 'static, S: HasIx<T>> HasIx<T> for Option<S> {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        self.iter_mut().for_each(|o| o.foreach_ix(&mut f))
    }
}
impl<T: 'static, S: HasIx<T>> HasIx<T> for Box<S> {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        self.as_mut().foreach_ix(&mut f);
    }
}
impl<T: 'static, S: HasIx<T>> HasIx<T> for &mut S {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        (*self).foreach_ix(&mut f);
    }
}
impl<T: 'static> HasIx<T> for Ix<T> {
    fn foreach_ix<'b, 'a: 'b, F>(&'a mut self, mut f: F)
    where
        F: FnMut(&'b mut Ix<T>),
    {
        f(self);
    }
}
