/*
 * @Copyright 2020 Jason Carr
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use alloc::vec::Vec;
use core::cell::RefCell;

use super::traverse;
use super::types::{Entry, MovingEntry, Region};
use crate::entry;
use crate::types::{HasIx, Ix, Root, SpotVariant};
use entry::Spot;
use traverse::*;

/*

Important invariants which must be maintined here:

If a spot has a forwarding pointer, then either a Cheney copy
is ongoing, or has just occured (and the source should be dropped),
or that location keeps a valid T element,
and can be recovered. This cannot be violated by non-compliant
access to T by users, as this cannot change whether a valid T
is present, only where that points.

The deep cloning methods rely on this invariant being upheld for
safety using packed headers, which do not tag whether a T value
is present.

This is maintained by callers of cheney_copy because they either
enable keeping T, or they drop
 */

impl<'a, T: 'static + HasIx<T>> Region<T> {
    #[cfg(feature = "debug-arena")]
    fn check_gen(ix: Ix<T>, internal: bool, old_gen: (u64, u64), new_gen: (u64, u64)) {
        {
            let prefix = if internal {
                "GC internal error (root)"
            } else {
                "GC"
            };
            if ix.nonce != old_gen.0 {
                if ix.nonce == new_gen.0 {
                    panic!("{}: Index processed twice", prefix);
                } else {
                    panic!("{}: Invalid source index for root", prefix);
                }
            } else if ix.generation < old_gen.1 {
                panic!(
                    "{}: Index is for a generation that is too old, it may have missed processing",
                    prefix
                );
            } else if ix.generation > old_gen.1 {
                panic!("{}: Index is for a generation that is too new, it may have been processed twice", prefix);
            }
        }
    }

    /**
     * Write a spot to the end of the destination
     * (at dst_ptr+len)
     * whenever the forward operation returns Some,
     * then we've written it to the end, and therefore the length can be incremented
     */
    unsafe fn push_spot(
        dst_ptr: *mut Spot<T>,
        cap: usize,
        len: usize,
        ix: Ix<T>,
        s: &mut Spot<T>,
        forward: &mut dyn FnMut(&mut Spot<T>, Ix<T>, Ix<T>) -> Option<Spot<T>>,
        #[cfg(feature = "debug-arena")] new_gen: (u64, u64),
    ) -> Option<Ix<T>> {
        if len >= cap {
            panic!("GC internal error: Not enough length to push spot during collection");
        }
        //NOTE: as a closure we're unable to mark
        //this as unsafe, but it is unsafe and should
        //always be called from an unsafe block
        let new_index = Ix::new(
            len,
            #[cfg(feature = "debug-arena")]
            new_gen.0,
            #[cfg(feature = "debug-arena")]
            new_gen.1,
        );

        let obj = forward(s, ix, new_index);
        obj.map(|obj| {
            let end = dst_ptr.add(len);
            core::ptr::write(end, obj);
            new_index
        })
    }

    // Perform a gc into a new destination vector. For efficiency,
    // the vector must have enough capacity for the new elements
    pub(crate) fn prim_gc_to<'b: 'a>(
        src: &mut [Spot<T>],
        dst: &'b mut Vec<Spot<T>>,
        roots: &mut Vec<Root<T>>,
        #[cfg(feature = "debug-arena")] old_gen: (u64, u64),
        #[cfg(feature = "debug-arena")] new_gen: (u64, u64),
    ) where
        T: HasIx<T>,
    {
        // safety NOTE: Necessary for safety of this method,
        // since we need to avoid a particular invalidation later
        // This means that dst should never move for safety

        let start = dst.len();

        //Push each root onto the destination, updating roots
        *roots = roots
            .drain(..)
            .filter_map(|root| {
                if !root.is_otherwise_rooted() {
                    None?
                }
                let ix = root.ix();
                #[cfg(feature = "debug-arena")]
                Self::check_gen(ix, true, old_gen, new_gen);

                let s = src.get_mut(ix.ix())?;
                unsafe {
                    root.cell.set(Self::push_spot(
                        dst.as_mut_ptr(),
                        dst.capacity(),
                        dst.len(),
                        ix,
                        s,
                        &mut |s, _, o| Some(s.move_to(o)),
                        #[cfg(feature = "debug-arena")]
                        new_gen,
                    ).expect("GC internal error: should pushed index should always be some"));
                    dst.set_len(dst.len() + 1);
                    assert!(dst.len() > start);
                }
                Some(root)
            })
            .collect();
        assert!(dst.len() > start || roots.is_empty());

        unsafe {
            let len = Self::cheney_copy(
                src.as_mut_ptr(),
                src.len(),
                dst.as_mut_ptr(),
                dst.capacity(),
                start,
                dst.len(),
                &mut |s, _, o| Some(s.move_to(o)),
                #[cfg(feature = "debug-arena")]
                old_gen,
                #[cfg(feature = "debug-arena")]
                new_gen,
            );
            dst.set_len(len);
        }
    }

    // See invariants at the top of this file
    unsafe fn cheney_copy(
        src: *mut Spot<T>,
        src_len: usize,
        dst: *mut Spot<T>,
        capacity: usize,
        start: usize,
        len: usize,
        forward: &mut dyn FnMut(&mut Spot<T>, Ix<T>, Ix<T>) -> Option<Spot<T>>,
        #[cfg(feature = "debug-arena")] old_gen: (u64, u64),
        #[cfg(feature = "debug-arena")] new_gen: (u64, u64),
    ) -> usize {
        //Start searching at the vector length before any roots
        let mut len = len;
        let mut obj_index = start;

        // preconditions, assert memory is readable
        if cfg!(miri) {
            for i in 0..len {
                core::mem::forget(
                    core::ptr::read_volatile(dst.add(i)));
            }
        }

        //Cheney copy starting at each of the roots
        while obj_index < len {
            if len > capacity {
                panic!("GC internal error: len past capacity");
            }

            let present = (*dst.add(obj_index)).get_mut().unwrap();
            let obj = present.get_mut();
            // NOTE for safety:
            // foreach_ix can panic,
            // therefore, length should never
            // be set until a valid object is in the location
            obj.foreach_ix(|pointed| {
                #[cfg(feature = "debug-arena")]
                Self::check_gen(*pointed, false, old_gen, new_gen);

                let pix = pointed.ix();
                if pix >= src_len {
                    panic!("GC internal error: invalid index in object during GC");
                }
                let s = &mut (*src.add(pix));
                match s.variant() {
                    SpotVariant::Present(_) => {
                        #[allow(unused)]
                        unsafe {
                            if let Some(new_ix) = Self::push_spot(
                                dst,
                                capacity,
                                len,
                                *pointed,
                                s,
                                forward,
                                #[cfg(feature = "debug-arena")]
                                new_gen,
                            ) {
                                (&mut *dst.add(len)).get_mut().unwrap();
                                *pointed = new_ix;
                                len += 1;
                            }
                        }
                    }
                    SpotVariant::BrokenHeart(new_index) => *pointed = new_index,
                }
            });
            obj_index += 1;
            assert!(len >= obj_index);
        }
        len
    }

    /**
     * Primitive depth-first search. We have to use *mut ... due to lots of overlap.
     *
     * If do_recover is true, then the caller must ensure that the destination of any
     * forwarding pointer encovered cannot be a location reached by the DFS
     */
    #[allow(unused)]
    pub(crate) unsafe fn prim_traverse<Str, State, V>(
        strat: Str,
        roots: &RefCell<Vec<Root<T>>>,
        src: *mut Spot<T>,
        len: usize,
        mark: bool,
        v: &mut V,
        do_recover: Option<*mut Spot<T>>,
        ix: Ix<T>,
        #[cfg(feature = "debug-arena")] gen: (u64, u64),
    ) where
        Str: traverse::Strategy<T, State>,
        V: Visitor<T, State>,
        T: HasIx<T> + 'static,
    {
        let mut to_ref = |ix: Ix<T>| -> Option<Entry<T>> {
            #[cfg(feature = "debug-arena")]
            ix.check_nonce(gen);

            // reads header, and then writes when we have unique access
            if ix.ix() >= len {
                panic!("GC: Invalid index in dfs")
            }
            let spot = src.add(ix.ix());
            if Spot::is_marked(spot) == Some(mark) {
                return None;
            }

            // We now have unique access.
            // We'll have to drop unique access to
            // the header before looping
            let spot_raw = spot;
            let spot = &mut *spot;
            match spot.variant() {
                SpotVariant::BrokenHeart(dest) => {
                    // we need to mark this behind a flag due
                    // to unsafety in spot.recover_kept
                    if let Some(dest_spots) = do_recover {
                        //must be disjoint
                        //by recover precondition
                        let other_spot = &mut *dest_spots.add(dest.ix());
                        let mut other_entry: entry::MutPresent<T> = match other_spot.variant() {
                            SpotVariant::Present(e) => e,
                            _ => unreachable!("GC internal error: Invalid state"),
                        };
                        spot.recover_kept(mark, other_entry.reborrow());
                    } else {
                        panic!("GC internal error: Invalid state")
                    }
                }
                _ => spot.mark(mark),
            }
            if let SpotVariant::Present(present) = (*spot_raw).variant() {
                Some(Entry { ix, roots, present })
            } else {
                None
            }
        };

        if let Some(start) = to_ref(ix) {
            strat.run(&mut to_ref, v, start);
        }
    }

    #[allow(unused)]
    pub(crate) fn traverse_base<'b, I, Str, State, Visit>(
        strat: Str,
        src: &'b mut [Spot<T>],
        start: I,
        roots: &RefCell<Vec<Root<T>>>,
        mut visit: Visit,
        #[cfg(feature = "debug-arena")] nonce: (u64, u64),
    ) where
        Str: traverse::Strategy<T, State>,
        Str: traverse::Strategy<T, traverse::PreOnly>,
        Visit: traverse::Visitor<T, State>,
        T: HasIx<T>,
        I: Iterator<Item = &'b Ix<T>> + Clone,
    {
        let mut no_v = PrePostVisitor {
            pre: no_process::<T>,
            post: (),
        };

        let ptr = src.as_mut_ptr();
        for ix in start.clone() {
            unsafe {
                Self::prim_traverse(
                    &strat,
                    roots,
                    src.as_mut_ptr(),
                    src.len(),
                    true,
                    &mut visit,
                    None,
                    *ix,
                    #[cfg(feature = "debug-arena")]
                    nonce,
                );
            }
        }
        for ix in start {
            unsafe {
                Self::prim_traverse(
                    &strat,
                    roots,
                    src.as_mut_ptr(),
                    src.len(),
                    false,
                    &mut no_v,
                    None,
                    *ix,
                    #[cfg(feature = "debug-arena")]
                    nonce,
                );
            }
        }
        let src1 = src;
    }
}

impl<T: 'static + HasIx<T>> Region<T> {
    unsafe fn cheney_clone<F>(
        &mut self,
        mut other: Option<&mut Self>,
        mut do_clone: F,
        start_ix: Ix<T>,
    ) where
        F: FnMut(MovingEntry<T>) -> Option<T>,
    {
        let dst_data = &Self::or_else(&mut other, self).data;
        let dst_cap = dst_data.capacity();
        let orig_len = dst_data.len();
        let dst = Self::or_else(&mut other, self).data.as_mut_ptr();
        #[cfg(feature = "debug-arena")]
        let nonces = {
            let r = Self::or_else(&mut other, self);
            (r.nonce, r.generation)
        };

        fn move_spot<'a, T>(
            roots: &'a RefCell<Vec<Root<T>>>,
            mut do_clone: impl FnMut(MovingEntry<T>) -> Option<T> + 'a,
        ) -> impl FnMut(&mut Spot<T>, Ix<T>, Ix<T>) -> Option<Spot<T>> + 'a {
            move |s: &mut Spot<T>, ix, dest| {
                Spot::move_to_keep_with(s, dest, |present| {
                    let entry = Entry { roots, present, ix };
                    let moving_entry = MovingEntry { entry, dest };
                    do_clone(moving_entry)
                })
            }
        }
        let (start, roots) = match other {
            Some(ref mut other) => (self.data.get_mut(start_ix.ix()).unwrap(), &other.roots),
            None => (self.data.get_mut(start_ix.ix()).unwrap(), &self.roots),
        };
        let should_inc = Self::push_spot(
            dst,
            dst_cap,
            orig_len,
            start_ix,
            start,
            &mut (move_spot(roots, &mut do_clone)),
            #[cfg(feature = "debug-arena")]
            nonces,
        ).is_some();
        let (src_len, src_ptr) = (self.data.len(), self.data.as_mut_ptr());
        #[cfg(feature = "debug-arena")]
        let (dst_nonce, dst_generation) = (
            Self::or_else(&mut other, self).nonce,
            Self::or_else(&mut other, self).generation,
        );
        let (dst_data, roots) = match other {
            Some(ref mut other) => (&mut other.data, &other.roots),
            None => (&mut self.data, &self.roots),
        };
        dst_data.set_len(orig_len + if should_inc {1} else {0});
        let len = Self::cheney_copy(
            src_ptr,
            src_len,
            dst_data.as_mut_ptr(),
            dst_data.capacity(),
            orig_len,
            dst_data.len(),
            &mut (move_spot(roots, do_clone)),
            #[cfg(feature = "debug-arena")]
            (self.nonce, self.generation),
            #[cfg(feature = "debug-arena")]
            (dst_nonce, dst_generation),
        );
        Self::or_else(&mut other, self).data.set_len(len);
    }

    fn or_else<'b, 'a: 'b, U>(opt: &'b mut Option<&'a mut U>, default: &'b mut U) -> &'b mut U {
        match opt {
            Some(r) => r,
            None => default,
        }
    }

    pub(crate) fn prim_clone_cstack_into<F>(
        &mut self,
        strat: impl traverse::Strategy<T, traverse::PreOnly>,
        mut other: Option<&mut Region<T>>,
        start: &mut Root<T>,
        mut do_clone: F,
    ) where
        F: FnMut(MovingEntry<T>) -> Option<T>,
    {
        let mut count = 0;
        unsafe {
            let (data, roots) = match other {
                Some(ref mut other) => (&mut self.data, &other.roots),
                None => (&mut self.data, &self.roots),
            };
            let mut v = PrePostVisitor {
                pre: |_: Entry<T>| count += 1,
                post: (),
            };
            let mut no_v = PrePostVisitor {
                pre: no_process::<T>,
                post: (),
            };
            Self::prim_traverse(
                &strat,
                roots,
                data.as_mut_ptr(),
                data.len(),
                true,
                &mut v,
                None,
                start.ix(),
                #[cfg(feature = "debug-arena")]
                (self.nonce, self.generation),
            );
            Self::prim_traverse(
                &strat,
                roots,
                data.as_mut_ptr(),
                data.len(),
                false,
                &mut no_v,
                None,
                start.ix(),
                #[cfg(feature = "debug-arena")]
                (self.nonce, self.generation),
            );
        }
        Self::or_else(&mut other, self).ensure(count);

        unsafe {
            self.cheney_clone(
                if let Some(r) = &mut other {
                    Some(r)
                } else {
                    None
                },
                do_clone,
                start.ix(),
            )
        };

        let ptr = self.data.as_mut_ptr();
        let start_ix = start.ix();
        // SAFETY:
        //
        // start.ix() is still valid because:
        // it was rooted, so it refers to an index which was valid
        // and we haven't invalidated any indexes with the cheney clones
        // and traversals above
        //
        // In addition, no borrows are active, so the location is safe
        // to read. Before editing make sure to make this clearer.
        let dest_ix = unsafe {
            // get the start destination before we fix-up
            // which would lose the destination
            Spot::is_forwarding(ptr.add(start_ix.ix()))
            // for accuracy, we need to ensure we don't move within here
            // BUT we can't root early here because the roots need to be
            // fixed up since they've been moved with the forwarding
        };

        let mut v = PrePostVisitor {
            pre: no_process::<T>,
            post: (),
        };

        let dest_ptr = Self::or_else(&mut other, self).data.as_mut_ptr();
        let (data, roots) = match other {
            Some(ref mut other) => (&mut self.data, &other.roots),
            None => (&mut self.data, &self.roots),
        };

        // SAFETY:
        //
        // data is a slice, so its pointer/length is valid
        unsafe {
            // NOTE: prim traverse doesn't allocate
            Self::prim_traverse(
                &strat,
                roots,
                data.as_mut_ptr(),
                data.len(),
                false,
                &mut v,
                Some(dest_ptr),
                start_ix,
                #[cfg(feature = "debug-arena")]
                (self.nonce, self.generation),
            );
        }
        // now finally check whether it's None
        // we *shouldn't* need to fix up if it is, but this is a bit
        dest_ix.map(|dest_ix| {
            // MUST be after the traverse above, since it fixes up
            // broken hearts
            *start = dest_ix.root(&Self::or_else(&mut other, self));
        });
    }
}
