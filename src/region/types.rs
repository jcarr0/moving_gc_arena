/*
 * @Copyright 2020 Jason Carr
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

use crate::entry::{MutPresent, Spot};
#[cfg(feature = "debug-arena")]
use crate::nonce;
use crate::types::{Ix, Root, Weak};
use alloc::vec::Vec;
use core::cell::RefCell;
use core::fmt;

/**
 * The type of a collectable region.
 *
 * This object can be used to allocate, collect,
 * traverse and update the objects within it.
 *
 * Access to the region is exposed through methods
 * on the corresponding reference types, and requires
 * references to this region in order to safely
 * reference the data within. This ensures that
 * garbage collections do not interrupt accesses and
 * vice versa, and allows for a conservative compile-time check for uniqueness, rather than
 * requiring use of an internal Cell type.
 *
 * Since garbage collection is a property of the region, it is not statically checked for indices.
 * Weak and Root will always be in sync with their
 * source region, but raw indices Ix may be invalidated.
 * Some methods (which necessarily take &mut self) may invalidate raw indices by moving the
 * objects, such as for a garbage collection.
 * These will be documented.
 *
 */
pub struct Region<T> {
    pub(crate) data: Vec<Spot<T>>,
    pub(crate) roots: RefCell<Vec<Root<T>>>,

    #[cfg(feature = "debug-arena")]
    pub(crate) nonce: u64,
    #[cfg(feature = "debug-arena")]
    pub(crate) generation: u64,
}

impl<T> Region<T> {
    #[inline]
    pub fn new() -> Self {
        Region {
            data: Vec::new(),
            roots: RefCell::new(Vec::new()),
            #[cfg(feature = "debug-arena")]
            nonce: nonce::next(),
            #[cfg(feature = "debug-arena")]
            generation: 0,
        }
    }
}
impl<T> Default for Region<T> {
    fn default() -> Self {
        Self::new()
    }
}
impl<T> fmt::Debug for Region<T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.write_str("(Region")?;
        #[cfg(feature = "debug-arena")]
        {
            f.write_str("(")?;
            self.nonce.fmt(f)?;
            f.write_str(", ")?;
            self.generation.fmt(f)?;
            f.write_str(")")?;
        }
        f.write_str(")")?;
        Ok(())
    }
}

/**
 * An entry inside the region, allowing
 * access to the underlying value and
 * the index, as well as Root and Weak creation.
 */
pub struct Entry<'a, T> {
    pub(crate) ix: Ix<T>,
    pub(crate) present: MutPresent<'a, T>,
    pub(crate) roots: &'a RefCell<Vec<Root<T>>>,
}

impl<'a, T: fmt::Debug> fmt::Debug for Entry<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        f.write_str("Entry { ix: ")?;
        self.ix.fmt(f)?;
        f.write_str(", entry: ")?;
        self.present.get().fmt(f)?;
        f.write_str("}")?;
        Ok(())
    }
}

impl<'a, T> Entry<'a, T> {
    #[inline]
    pub fn root(&self) -> Root<T> {
        let i = self.ix;
        let r = self.present.root(i);
        if !r.is_otherwise_rooted() {
            self.roots.borrow_mut().push(r.clone());
        };
        r
    }

    #[inline]
    pub fn weak(&self) -> Weak<T> {
        self.present.weak(self.ix)
    }
    #[inline]
    pub fn ix(&self) -> Ix<T> {
        self.ix
    }
    #[inline]
    pub fn get(&self) -> &T {
        self.present.get()
    }
    #[inline]
    pub fn get_mut(&mut self) -> &mut T {
        self.present.reborrow().get_mut()
    }
    /**
     * Re-borrows this entry for a new lifetime
     */
    #[inline(always)]
    pub fn reborrow(&mut self) -> Entry<T> {
        let Entry { ix, present, roots } = self;
        let present = present.reborrow();
        Entry {
            ix: *ix,
            present,
            roots,
        }
    }
}

/**
 * An Entry with additional information about the destination,
 * so that the source entry can be updated to reference the
 * new cloned location
 */
pub struct MovingEntry<'a, T> {
    pub(crate) entry: Entry<'a, T>,
    pub(crate) dest: Ix<T>
}

impl<'a, T: 'a> core::ops::Deref for MovingEntry<'a, T> {
    type Target = Entry<'a, T>;
    fn deref(&self) -> &Self::Target {
        &self.entry
    }
}
impl<'a, T> core::ops::DerefMut for MovingEntry<'a, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.entry
    }
}

impl<'a, T> MovingEntry<'a, T> {
    /**
     * Back-patch the source entry using the destination index. This can
     * be used so that the source entry now refers to this new index.
     *
     * In addition, the index can be safely stored in an external data structure.
     * This should not be used for the returned value, instead see `Entry::ix`.
     * See the `get_raw_destination_ix` method for details about what is allowed.
     */
    #[inline(always)]
    pub fn backpatch(&mut self, f: impl FnOnce(&mut Entry<'a, T>, Ix<T>)) {
        f(&mut self.entry, self.dest)
    }

    /**
     * Returns the future index for this entry immediately, although it will
     * not be valid. Incorrect usage of this method will result in panics.
     *
     * This cannot be stored in the destination of the clone, but can be stored
     * in the source. To store in the destination, instead call the Entry::ix
     * method, as this will be rewritten in the final entry.
     *
     * This index should be safe so long as it is not used in the destination of
     * the clone. That is, the safe operations allow:
     * 
     * * Storing in the source entry
     * * Storing in an external data structure (indices are not invalidated
     * after the start of the clone)for instance:
     *
     * It is not possible at this time to store the true source index into the
     * destination.
     */
    #[inline(always)]
    pub fn get_raw_destination_ix(&self) -> Ix<T> {
        self.dest
    }

    /**
     * Re-borrows this entry for a lifetime.
     */
    #[inline(always)]
    pub fn reborrow(&mut self) -> MovingEntry<T> {
        MovingEntry {
            entry: self.entry.reborrow(),
            dest: self.dest,
        }
    }

    pub fn to_entry(self) -> Entry<'a, T> {
        self.entry
    }
}
