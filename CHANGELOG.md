# Changelog

## Unreleased

### Changed

- Cloning methods now take a function which can return `Option<T>`. On a None return, the value is unchanged. Some methods now also allow more flexible access to the destination index.

## [0.3.3] - 2021-03-26

### Added
Added a method to format the contents of an Ix,
and improved debug output in other contexts

## [0.3.2] - 2020-11-21

### Fixed
Fix a long-standing soundness issue which could occur on invalid indices during garbage collections.

## [0.3.1] - 2020-06-26

### Fixed
Re-enable no_std, which was accidentally added in a commit

## [0.3.0] - 2020-06-26

More consistent and more configurable methods

### Changed
- Renamed MutEntry to Entry
- All callback functions in the traversal suite now grant an Entry (formally MutEntry) instead of an Ix<T>, &mut T pair.
    This enables creating Roots and Weaks from the entry.
- Traversals now can use a customized strategy, enabling customization
    for other orders than the one from HasIx, and for other memory options besides the call stack.
- Weak::ix now panics on failure, following the other methods. Use try_ix to get an Ix faillably
- Weak::root now correctly borrows instead of taking self. This could be avoided by cloning but is a small annoyance.
- Weak::root now takes a reference to the region to root in.

### Fixed
- Fixed an issue where roots created by Weak::root could fail to be associated with a Region if that index had never been rooted since the last garbage collection. Ix::root unaffected.

### Removed
- Removed deprecated MutEntry::{as_ref, as_mut_ref}

### Internal
- Improved the test suite

## [0.2.6 (Hotfix)] - 2020-05-11

### Fixed
- The deep clone into methods are fixed to use the correct region for
the resulting Root.
- Traversals now check the region of indices with debug-arena enabled

## [0.2.5] - 2020-05-10

### Added
- Added a variety of deep cloning methods for objects.

### Fixed
- Debug information was included on release builds by default.

### Changed
- [Internal] Safety improvements on collection and support for later changes.

## [0.2.4] - 2020-04-19

### Added
- All indices now implement equality. While this no-longer imposes the guardrail of the extra
required method call, safety can be tested using debug-arena.

## [0.2.3] - 2020-02-16

### Added
- Roots and weak pointers can be created at any time with an immutable reference, rather than just at
entry creation time. This also means that some existing uses no longer require mutable references.
    + Different index types can be converted with ix, weak or root, and try versions of these
      operations that return results, when applicable. Weak::ix still returns Option in this version.
- Added missing Debug implementations for Region, MutEntry, and improved other Debug implementations.
- Added a simple variant of a depth-first traversal: `Region::dfs_mut_cstack`.

## [0.2.2] - 2020-02-05

### Added
- Now no_std, since features depended only on alloc/core.
- [Internal] Added CI and migrated to Gitlab

### Changed
- [Internal] Refactored project organization

## [0.2.1] - 2020-01-24

### Added
- The feature "packed-headers" can be enabled for an experimental object layout featuring reduced header size.
- Further improved documentation.

### Changed
- Deprecated the confusing MutEntry::{as_ref, as_mut_ref}; Use {get, get_mut} instead

## [0.2.0] - 2020-01-11
### Added
- Added an optional feature to enable debugging index validity at the cost of efficiency
    + Accesses to regions will be checked and give correct errors
    + Validity will be checked during GC
    + This has a dramatic increase in space cost, and a small overhead in time.
- Ix<T> supports an identifier method, which returns a usize,
    unique for the current region/generation
- Added and improved documentation in several places

### Changed
- Separated Weak and Root into two different types.
    + Weak.ix() now returns Option<Ix<T>>, which can be used to test
      if it's been collected
- MutEntry::to_root is now MutEntry::root and no longer takes ownership

### Fixed
- Creating a weak and root pointer to the same entry would cause Weak pointers
to act like roots.

## [0.1.1] - 2020-01-07
### Added
- Ex<T> now is Clone regardless of T
- Cargo.toml links to repository
### Fixed
- lib.rs links to docs correctly

## [0.1.0] - 2020-01-06
Initial release

